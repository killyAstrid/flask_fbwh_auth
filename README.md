# authenticate requests coming from facebook webhooks

## What YOU need to do
set FACEBOOK_APP_SECRET in your flask app config

## What WE do
check the incoming encrypted payload key against the expected value

## HOW?
Facebook add a X-Hub-Signature header to each webhook request. This header is 
the body/payload of the request sha1 encrypted with your fb secret app key.