import hashlib
import hmac

from flask import request, abort, current_app as app

def fb_wh_auth(f):
    def decorator(*args, **kwargs):
        signature = request.headers.get('X-Hub-Signature')
        if not signature or len(signature) < 6:
            abort(403)
        signature = signature[5:]
        expected_hash = hmac.new(
            app.config['FACEBOOK_APP_SECRET'].encode(),
            request.get_data(),
            hashlib.sha1
        )
        if not hmac.compare_digest(expected_hash.hexdigest(), signature):
            abort(401)
        return f(*args, **kwargs)
    return decorator
