
from setuptools import setup


setup(
    name='Flask-FacebookWebhook-Auth',
    version='1.0',
    license='BSD',
    author='Abel Stam',
    author_email='abelstam@hotmail.com',
    description='Authenticate requests coming from facebook webhooks',
    py_modules=['flask_fbwh_auth'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'Flask',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)